<?php

/*
|--------------------------------------------------------------------------
| Guest Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'Guest\GuestController@index')->name('index');



/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
*/
Auth::routes();

Route::get('/home', 'User\HomeController@index')->name('home');





/*
|--------------------------------------------------------------------------
| ADMIN Routes
|--------------------------------------------------------------------------
*/

//admin login
Route::GET('/admin', 'Auth_Admin\LoginController@showLoginForm')->name('admin.login');
Route::POST('/admin', 'Auth_Admin\LoginController@login');

//admin confirm password
Route::GET('/admin/password/confirm', 'Auth_Admin\ConfirmPasswordController@showConfirmForm')->name('admin.password.confirm');
Route::POST('/admin/password/confirm', 'Auth_Admin\ConfirmPasswordController@confirm ');

//email
Route::POST('/admin/password/email', 'Auth_Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::GET('/admin/password/reset', 'Auth_Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');

//reset password
Route::GET('/admin/password/reset/{token}', 'Auth_Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
Route::POST('/admin/password/reset', 'Auth_Admin\ResetPasswordController@reset')->name('admin.password.update');

//admin logout
Route::POST('/admin/logout', 'Auth_Admin\LoginController@logout')->name('admin.logout');

//admin home
Route::GET('/admin/home', 'Admin\AdminController@index')->name('admin.home');