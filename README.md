<p align="center"><img src="https://farm1.static.flickr.com/433/19217831873_3cd730123f_b.jpg" width="400"></p>


multiauth system by Faisal Ahamed
## About Project
Two types of User
                1.User
                2.Admin
Two Database Tabel:
                1.Users
                2.Admins
                
## Functionality

1.two login page for two types of user
2. two types of password reset system via email

## Installing package

git clone  https://gitlab.com/faisalahamed/laravel-multiauth-system.git
composer install

configure .env file

php artisan migrate:fresh --seed

and enjoy.

admin login link: {localhost}/admin

username:admin@xfaisal.com
password:admin1234
