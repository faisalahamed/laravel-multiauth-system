<?php

namespace App\Http\Controllers\Auth_Admin;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }


    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
//    public const HOME = 'admin/home';
//    protected $redirectTo = RouteServiceProvider::HOME;

    protected $redirectTo='admin/home';

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth_admin.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }
    public function broker()
    {
        return Password::broker('admins');
    }


}
