<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">

<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/mdb.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/mdb.lite.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/style.css')}}" rel="stylesheet">