<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--    <title>{{ config('app.name', 'Laravel Multi Auth System') }}</title>--}}
    @yield('title')


    @include('includes.styles')
    @yield('styles')


</head>
<body>
<div id="main">
    <div id="nav-bar">
        @include('includes.admin-nav-bar')
    </div>

    <div id="content">
        @yield('content')
    </div>

</div>

<div id="footer">
    @include('includes.scripts')
    @yield('scripts')
</div>

</body>
</html>
