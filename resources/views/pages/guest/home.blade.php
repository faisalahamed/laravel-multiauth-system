@extends('layouts.guest')
@section('styles')
    <style>
        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
@endsection

@section('title')
@endsection


@section('content')

    <div class="content">
        <div class="title m-b-md">
            Laravel
        </div>
    </div>

@endsection
