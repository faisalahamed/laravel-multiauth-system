<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        App\Models\Admin::create([
            'name' => 'AdminFaisal',
            'email' => 'admin@xfaisal.com',
            'password' => bcrypt('admin1234')
        ]);
        App\Models\User::create([
            'name' => 'UserFaisal',
            'address' => 'Dhaka',
            'email' => 'user@xfaisal.com',
            'password' => bcrypt('user1234')
        ]);
    }
}
